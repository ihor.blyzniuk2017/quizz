import React, {Component} from "react";
import ActiveQuizz from "../../components/ActiveQuizz/ActiveQuizz";
import FinishedQuizz from "../../components/FinishedQuizz/FinishedQuizz";
import classes from './Quizz.module.css'

export default class Quizz extends Component {
	state = {
		results: {},
		isFinished: false,
		activeQuestion: 0,
		answerState: null,
		quizz: [
			{
				question: 'What language do you have to speak?',
				rightAnswerId: 4,
				id: 1,
				answers: [
					{text: 'Ukrainian', id: 1},
					{text: 'Russian', id: 2},
					{text: 'Spanish', id: 3},
					{text: 'English', id: 4},
				]
			},
			{
				question: 'What programming language do you use?',
				rightAnswerId: 1,
				id: 2,
				answers: [
					{text: 'JavaScript', id: 1},
					{text: 'C#', id: 2},
					{text: 'PHP', id: 3},
					{text: 'Python', id: 4},
				]
			}
		]
	}

	onAnswerClickHandler = (answerId) => {

		if(this.state.answerState){
			const key = Object.keys(this.state.answerState)[0];
			if(this.state.answerState[key] === 'success'){
				return
			}
		}

		const question = this.state.quizz[this.state.activeQuestion];
		const results = this.state.results;


		if(question.rightAnswerId === answerId){

			if(!results[question.id]) {
				results[question.id] = 'success'
			}

			this.setState({
				answerState: {[answerId] : 'success'},
				results
			})
			const timeout = window.setTimeout(()=>{
				if(this.isQuizzFinished()){
					this.setState({
						isFinished: true
					})
				}else{
					this.setState({
						activeQuestion: this.state.activeQuestion + 1,
						answerState: null
					})
				}
				window.clearTimeout(timeout);
			}, 1000)
			
		}else{
			results[question.id] = 'error';
			this.setState({
				answerState: {[answerId] : 'error'},
				results
			})
		}
	}

	isQuizzFinished() {
		return this.state.activeQuestion + 1 === this.state.quizz.length;
	}

	retryHandler = () => {
		this.setState({
			results: {},
			isFinished: false,
			activeQuestion: 0,
			answerState: null
		})
	}
	render() {
		return(
			<div className={classes.Quizz}>
				<div className={classes.QuizzWrapper}>
					<h1>Quizz</h1>
					{
						this.state.isFinished 
						? <FinishedQuizz
							results = {this.state.results}
							quizz = {this.state.quizz}
							onRetry = {this.retryHandler}
						  />
						: <ActiveQuizz
						answers = {this.state.quizz[this.state.activeQuestion].answers}
						question = {this.state.quizz[this.state.activeQuestion].question}
						onAnswerClick = {this.onAnswerClickHandler}
						quizzLength = {this.state.quizz.length}
						answerNumber = {this.state.activeQuestion + 1}
						state = {this.state.answerState}
						/>
					}
					
				</div>
			</div>
		)
	}
}