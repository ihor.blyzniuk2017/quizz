import React from "react";
import AnswersList from "./AnswersList/AnswersList";
import classes from "./ActiveQuizz.module.css"

const ActiveQuizz = (props) => {
	return (
		<div className={classes.ActiveQuizz}>
			<p className={classes.Question}>
				<span>
					<strong>{props.answerNumber}.</strong>&nbsp;
					{props.question}
				</span>
				<small>{props.answerNumber} from {props.quizzLength}</small>
			</p>
			<AnswersList
				answers={props.answers}
				onAnswerClick={props.onAnswerClick}
				state = {props.state}
			/>
		</div>
	)
}

export default ActiveQuizz;