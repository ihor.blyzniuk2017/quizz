import React from "react";
import classes from "./FinishedQuizz.module.css";

const FinishedQuizz = (props) => {

	const successCount = Object.keys(props.results).reduce((total, key)=>{
		if(props.results[key] === 'success'){
			total++
		}
		return total;
	}, 0);

	return(
		<div className={classes.FinishedQuizz}>
			<ul>
				{props.quizz.map((quizzItem, index) => {

					const cls = [
						'fa',
						props.results[quizzItem.id] === 'error' ? 'fa-times' : 'fa-check',
						classes[props.results[quizzItem.id]]
					]

					return (
						<li 
							key={index}
						>
							<strong>{index + 1}</strong>.&nbsp;
							{quizzItem.question}
							<i className={cls.join(' ')}/>
						</li>
					)
				})}
				
			</ul>
			<p>Right {successCount} from {props.quizz.length}</p>
			<div>
				<button onClick={props.onRetry}>return</button>
			</div>
		</div>
	)
}

export default FinishedQuizz;