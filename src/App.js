import React from 'react';
import Layout from './hoc/Layout/Layout';
import Quizz from './containers/Quizz/Quizz';

function App() {
  return (
    <Layout>
      <Quizz/>
    </Layout>

  );
}

export default App;
